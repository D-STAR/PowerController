# KC8ARJ Power Control Firmware

This repository contains the firmware for the power control box.  The
Raspberry Pi sends a signal to the PIC.  The PIC then waits long enough
for the Pi to shutdown, interrupts power for a few seconds, then re-applies
power.

---

![State Transition](diagrams/StateTransitions.png)

**Firmware States**

---

There are three functions in three C files in thie repo.  The bulk of
the logic is in the mainline.  An initialization function sets up the
ports and initializes the timer and some counters.

There are several branches representing different levels of development:

* **Proto1** simply provides signalling to the relays to evaluate the relay control circuitry

* **Proto2** has most of the logic working and tested

* **Proto3** breaks down the long main file into individual functions and significantly improves documentation.  The files are:
  * `Proto3.c` - Mainline
  * `Initialization.c` - Initialization
  * `T1interrupt.c` - Timer 1 interrupt service routine
  
* **PowerController** has the same code as Proto3 but includes a `refman.pdf`
  produced by Doxygen with some comment improvements to make the refman
  more readable.




 
